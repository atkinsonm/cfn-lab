# GitLab CI Templates

## Prerequisites

### Lite Version

The only prerequisite to using the lite version of the pipeline is having a GitLab project which will contain your CloudFormation code. All CI jobs used by the lite version do not require AWS access, and so an AWS account and a custom GitLab runner are not required.

### Full Versions

If you intend to use the full version of the CI pipelines (for either CloudFormation stacks or CloudFormation stack sets), you will first need:

1. A GitLab project which will contain your CloudFormation code.
2. At least one AWS account.
3. A custom GitLab runner with permissions to manage CloudFormation resources. See the [gitlab-runner-dind](https://code.chs.usgs.gov/usgs-chs/CHS-IaC/baseline/aws-service-catalog/custom-sc/GitLab-Runner-DinD/blob/master/README.md) Service Catalog product for code to launch a pre-designed GitLab runner solution.

## Templates

### [.gitlab-ci-aws-cfn.yml](.gitlab-ci-aws-cfn.yml)

This template is designed to be a complete CI/CD pipeline for a CloudFormation stack.

```yaml
include:
  - remote: https://gitlab.com/jhctech-oss/aws-tools/cfn-lab/raw/v3.1.2/templates/.gitlab-ci-aws-cfn.yml
```

Substitute desired version as needed.

### [.gitlab-ci-aws-cfn-lite.yml](.gitlab-ci-aws-cfn-lite.yml)

This template is designed to launch a CI/CD pipeline for essential static code testing of CloudFormation templates.

```yaml
include:
  - remote: https://gitlab.com/jhctech-oss/aws-tools/cfn-lab/raw/v3.1.2/templates/.gitlab-ci-aws-cfn-lite.yml
```

Substitute desired version as needed.

For the lite version, only the following jobs are included:

1. `unit:cfn-lint`
2. `security:cfn_nag`
3. `qa:static:yaml`
4. `qa:static:json`

See the `Unit Test` and `Quality Assurance (QA)` sections below for full descriptions of the associated CI jobs.

### [.gitlab-ci-aws-cfn-stackset.yml](.gitlab-ci-aws-cfn-stackset.yml)

This template is designed to be a complete CI/CD pipeline for a CloudFormation stack set.

```yaml
include:
  - remote: https://gitlab.com/jhctech-oss/aws-tools/cfn-lab/raw/v3.1.2/templates/.gitlab-ci-aws-cfn-stackset.yml
```

Substitute desired version as needed.

#### Testing Stack Sets

The stack set CI jobs are designed to inherit most controls from [.gitlab-ci-aws-cfn.yml](.gitlab-ci-aws-cfn.yml). CloudFormation does not natively support change sets or drift detection for stack sets, so we recommend that you still set all environment variables expected by [.gitlab-ci-aws-cfn.yml](.gitlab-ci-aws-cfn.yml) and select a single stack instance to represent the whole when running drift detection and change sets.

#### Use Cases and Assumptions

Currently, the stack set CI assumes that all stack instances are mirrors of one another. The following cases are currently not supported:

* Per-stack-instance parameter overrides
* Stack instances deployed variably across regions and accounts (i.e. regions: A, B for account 1, region B for account 2, region C for account 3)

Two variables dictate the deployment of stack instances: `CFN_STACK_SET_ACCOUNTS` and `CFN_STACK_SET_REGIONS`. On any given pipeline/environment, these values will be treated as the source of truth. Any accounts or regions not present in these variables will be deleted.

#### Execution Flow

1. Check to see if a stack set with this name exists in the target account.
    1. If it does not, create the stack set, then create all stack instances based on `CFN_STACK_SET_ACCOUNTS` and `CFN_STACK_SET_REGIONS`.
    2. If it does exist, perform the following steps:
        1. Delete any stack instances not present in `CFN_STACK_SET_ACCOUNTS` and `CFN_STACK_SET_REGIONS`
        2. Update remaining stack instances based on the latest template and other parameters
        3. Add additional stack instances for any present in `CFN_STACK_SET_ACCOUNTS` and `CFN_STACK_SET_REGIONS` but not the stack set

## Stages

The basic flow of the pipeline is:

### 1. Build

* Perform drift detection on the CloudFormation stack. If drift detection fails, the underlying resources have deviated and might not be able to be updated by CloudFormation until they are brought back in-sync.

### 2. Unit Test

* `unit:cfn-lint` - Uses [`cfn-python-lint`](https://github.com/awslabs/cfn-python-lint) to check syntax of CloudFormation templates.
  * only runs if files under the `cloudformation` folder have changed according to the diff
* `security:cfn_nag` - Uses [cfn-nag](https://github.com/stelligent/cfn_nag) to perform static security testing on CloudFormation templates against AWS best practices
  * Only runs if files under the `cloudformation` folder have changed according to the diff
  * Job will fail on any findings (warnings or failures). Recommended approaches are to resolve or [suppress](https://github.com/stelligent/cfn_nag#per-resource-rule-suppression) findings.
  * Findings can be viewed in the artifacts or job console output.

### 3. Integration Test

* Package CloudFormation templates and upload to a temporary location in S3.
* Create a change set to evaluate the CloudFormation stack for changes determined by the new template. output them to the CI pipeline.
* Export artifacts `additions.json`, `removals.json`, `modifications.json`, `imports.json` based on resource changes expected.
* Fails if the change set cannot be created, passes if the change set is created or fails due to no resource changes.
* Supplies stop actions which, if triggered, delete the change set and temporary S3 bucket
  * Stops automatically after fixed time intervals to periodically clean up old change sets and S3 buckets. Cleanup will not impact deployed stacks or executed change sets. `auto_stop_in` times are:
    * Development: 1 day (low because of high anticipated dev/test cycles in this environment)
    * Staging: 3 days (increased to allow additional review time)
    * Production: 1 week (increased to allow additional review time)

### 4. Quality Assurance (QA)

* `qa:static:yaml` - Performs `yamllint` on yaml files with `.yml` or `.yaml` extensions. Runs on local files rather than the S3-hosted files.
  * only runs if files with `.yml` or `.yaml` extensions have changed according to the diff
* `qa:static:json` - Performs JSON syntax error detection on any files with the `.json` extension. Runs on local files rather than the S3-hosted files.
  * only runs if files with `.json` extensions have changed according to the diff
* Uses the `estimate-template-cost` awscli command to build a simple monthly calculator link estimating the costs of the resources.

### 5. Deploy

* Executes the change set created during the integration test stage.
  * Execution mode configurable by setting the `DEPLOY` environment variable:
    * `manual` (default): equivalent to [`when: manual`](https://docs.gitlab.com/ee/ci/yaml/#whenmanual)
    * `automated`, `continuous`, or `on_success`: equivalent to [`when: on_success`](https://docs.gitlab.com/ee/ci/yaml/#when)
    * any other value: skip deploy stage
* Supplies a stop action to delete the CloudFormation stack on manual trigger.

## Environments

Jobs are configured to run on AWS EC2 instances and use the instances' underlying instance profiles to authenticate to AWS APIs. This means that the default behavior is to launch infrastructure in the same AWS account as the runner instances. To accommodate separation of these instances, jobs that interface with AWS APIs expect runners to be tagged with `dev` or `prod`.

Default job behaviors are scripted into the template to support a master/feature branch development workflow. Below are the default environments:

* dev/development: all branches except master
  * we assume that the master branch is the production branch
  * run on runners tagged `dev`
* staging: merge_requests
  * we want to evaluate change sets against `prod` during a merge request to make sure we have completed all checks before merging
  * staging delivery/deployment is isolated from prod
  * a change set is run against the prod CloudFormation stack but is deleted during cleanup to avoid accidental execution
  * run on runners tagged `prod`
* prod/production: the master branch, scheduled pipelines, tagged refs
  * we assume that only production releases utilize git tags
  * run on runners tagged `prod`

The delivery/deployment jobs in these environments can be customized by overriding the `(DEV|STAGING|PROD)_ENV` environment variables, respectively.
