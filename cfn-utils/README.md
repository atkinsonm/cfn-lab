# CloudFormation Deployment Project

Contains scripts for performing CloudFormation stack creation and update in the `deploy` phase based on the current state of the stack. Also contains scripts useful for performing change management, `test`ing, and staging prior to a deployment.

## Software Dependencies

1. awscli
2. [cfn-tail](https://github.com/taimos/cfn-tail)

## Scripts

Scripts are categorized with a type and a stage. The definitions are below:

### Types

* **Type:** CI: this script is primarily used to execute CI jobs
* **Type:** Utility: this script is used by one or multiple CI or Utility scripts as a way to reuse and modularize code segments
* **Type:** CI/Utility: this script defines a function which can be used as a utility by other scripts and also has use cases as a standalone CI job

For use of these scripts outside of the `awslint` package: since Utility scripts define a function, the script must be sourced (`. ./script_name.sh`) so the function name is available to the bash session, and then the function name can be called to execute (`script_name`).

### Stages

`CI` or `CI/Utility` script types are designed to be used in one or more stages:

* `integration test` - jobs designed to test the combined functionality of CloudFormation templates and other resources
* `staging` - jobs designed to make CloudFormation templates and other deployment artifacts available for deployment ("Continuous Delivery")
* `deploy` - jobs designed to make changes to deploy a new CloudFormation stack or perform in-place updates on an existing stack ("Continuous Deployment")
* `cleanup` - jobs designed to eliminate any leftover resources from prior stages post-deployment

### List of Scripts

#### [delete_change_set.py](delete_change_set.py)

Deletes a change set; useful for cleaning up after evaluating a change set if the change set will not be executed in this pipeline
**Type:** CI/Utility
**Stage:** `cleanup`

##### Required Parameters for delete_change_set

* `--stack-name`

##### Optional Parameters for delete_change_set

* `--change-set-name`

#### [delete_stack.py](delete_stack.py)

Deletes a stack; sets termination protection to false before deletion. **Use with caution** as this operation cannot be interrupted or undone.
**Type:** CI/Utility
**Stage:** `cleanup`

#### Required Parameters for delete_stack

* `--stack-name`

#### [execute_change_set.py](execute_change_set.py)

Executes a previously-created change set, providing console output of the stack events until some end state is reached.
**Type:** CI/Utility
**Stage:** `deploy`

#### Required Parameters for execute_change_set

* `--stack-name`

##### Optional Parameters for execute_change_set

* `--change-set-name`

## Parameters

We recommend defining these either in your .gitlab-ci.yml as `variables` or as project-level [secret variables](https://docs.gitlab.com/ee/ci/variables/#variables). The main requirement is that the below variables are available to the jobs requiring them. The "required by" and "used by" designate the minimum job set where the variables need to be available.

The [CI templates](../templates/README.md) will use these parameter names by default, and some have default values. We recommend overriding these in your `.gitlab-ci.yml` under your `include` statement or (better yet) configuring them as project-level secret variables which can be scoped to an environment (both of which [take precedence](https://docs.gitlab.com/ee/ci/variables/#priority-of-environment-variables) over the default values the `include` file might provide).

### Required

1. CFN_BASE_TEMPLATE
   * **Required by:** [create_change_set.sh](create_change_set.sh)
   * **Description:** path to main template in the form of filename or template url beginning with `https://BUCKETNAME.s3`
2. CFN_STACK_NAME
   * **Required by:** all jobs
   * **Description:** the CloudFormation stack name to assign or the name of an existing stack

### Optional

1. CFN_CHANGE_SET_NAME
   * **Used by:** [create_change_set.sh](create_change_set.sh), [delete_change_set.py](delete_change_set.py)
   * A generic name will be chosen if this parameter is not set
