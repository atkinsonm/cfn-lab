#! /usr/bin/python3
"""Command line arguments and execution for deleting CloudFormation stack sets."""

import argparse
import deploy

PARSER = argparse.ArgumentParser()
PARSER.add_argument('--stack-set-name',
                    '-s',
                    help="the name of the CloudFormation stack set",
                    type=str)
PARSER.add_argument('--operation-preferences',
                    '-o',
                    default='MaxConcurrentPercentage=100,FailureToleranceCount=0',
                    help="operation preferences for stack set updates",
                    type=str)

ARGS = PARSER.parse_args()
STACK_SET_NAME = ARGS.stack_set_name
OPERATION_PREFERENCES = ARGS.operation_preferences

D = deploy.StackSet(STACK_SET_NAME)
D.delete_stack_set(OPERATION_PREFERENCES)
