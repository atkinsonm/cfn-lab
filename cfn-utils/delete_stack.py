#! /usr/bin/python3
"""Command line arguments and execution for deleting CloudFormation stacks."""

import argparse
import deploy

PARSER = argparse.ArgumentParser()
PARSER.add_argument('--stack-name',
                    '-s',
                    help="the name of the CloudFormation stack",
                    type=str)

# print(parser.format_help())
# usage: test_args_4.py [-h] [--foo FOO] [--bar BAR]
# optional arguments:
#   -h, --help         show this help message and exit
#   --foo FOO, -f FOO  a random options
#   --bar BAR, -b BAR  a more random option

ARGS = PARSER.parse_args()
STACK_NAME = ARGS.stack_name

D = deploy.Stack(STACK_NAME)
D.delete_stack()
