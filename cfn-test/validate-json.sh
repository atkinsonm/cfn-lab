#!/bin/bash
ERROR_COUNT=0;

echo "Validating JSON files..."

# Loop through the JSON files in this repository
for JSON in $(find . -name '*.json'); do 

    # Validate the JSON with jq
    if ERRORS=$(jq -e . "$JSON" 2>&1 >/dev/null); then
        echo "[pass] $JSON";
    else
        ((ERROR_COUNT++));
        echo "[fail] $JSON: $ERRORS";
    fi; 
    
done; 

echo "$ERROR_COUNT json validation error(s)"; 
if [ "$ERROR_COUNT" -gt 0 ]; 
    then exit 1; 
fi
