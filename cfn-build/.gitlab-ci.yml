---

# detect_drift when stack exists with drift
# expected result: job fails
unit:detect_drift:
  stage: unit test
  variables:
    CFN_BASE_TEMPLATE: https://${CFN_STAGING_S3_BUCKET}.s3.amazonaws.com${CFN_STAGING_S3_PREFIX}S3Bucket.yml
    CFN_STACK_NAME: drift-stack-$CI_COMMIT_SHORT_SHA
    CFN_STAGING_S3_BUCKET: ${CI_COMMIT_SHORT_SHA}-${CI_PIPELINE_IID}-temp-bucket-drift # unique name to avoid collisions
    CFN_BASE_TEMPLATE_NAME: S3Bucket.yml
  script:
    - cfn-build/cfn_package.sh $CFN_STAGING_S3_BUCKET $CFN_STAGING_S3_PREFIX $CFN_TEMPLATE_PATH $CFN_BASE_TEMPLATE_NAME
    - cfn-deploy/create_change_set.sh || true
    - python3 cfn-utils/execute_change_set.py --stack-name $CFN_STACK_NAME --change-set-name $CFN_CHANGE_SET_NAME
    # simulate a drift scenario
    - bucketName=$(aws cloudformation describe-stack-resources
      --stack-name $CFN_STACK_NAME
      --logical-resource-id S3Bucket
      --query "StackResources[?LogicalResourceId == 'S3Bucket'].PhysicalResourceId"
      --output text)
    - aws s3 rb s3://$bucketName
    # detect the drift
    - |
      {
        python3 cfn-utils/detect_drift.py --stack-name $CFN_STACK_NAME
        driftExitCode=$?
      } || {
        echo "Something went wrong."
      }
    - |
      if [ "$driftExitCode" != "1" ]; then
        echo "Something went wrong in drift detection unit testing.";
        exit 1;
      fi

# detect_drift when no stack is present
# expected result: job passes with note about not finding a stack
unit:detect_drift_no_stack:
  stage: unit test
  variables:
    CFN_STACK_NAME: no-stack-$CI_COMMIT_SHORT_SHA
  script: python3 cfn-utils/detect_drift.py --stack-name $CFN_STACK_NAME

# detect_drift when a stack exists but no drift exists
# expected result: job passes
unit:detect_no_drift:
  stage: unit test
  variables:
    CFN_BASE_TEMPLATE: https://${CFN_STAGING_S3_BUCKET}.s3.amazonaws.com${CFN_STAGING_S3_PREFIX}S3Bucket.yml
    CFN_STACK_NAME: no-drift-stack-$CI_COMMIT_SHORT_SHA
    CFN_STAGING_S3_BUCKET: ${CI_COMMIT_SHORT_SHA}-${CI_PIPELINE_IID}-temp-bucket-no-drift # unique name to avoid collisions
    CFN_BASE_TEMPLATE_NAME: S3Bucket.yml
  script:
    - cfn-build/cfn_package.sh $CFN_STAGING_S3_BUCKET $CFN_STAGING_S3_PREFIX $CFN_TEMPLATE_PATH $CFN_BASE_TEMPLATE_NAME
    - cfn-deploy/create_change_set.sh || true
    - python3 cfn-utils/execute_change_set.py --stack-name $CFN_STACK_NAME --change-set-name $CFN_CHANGE_SET_NAME
    # check for drift
    - |
      {
        python3 cfn-utils/detect_drift.py --stack-name $CFN_STACK_NAME
        driftExitCode=$?
      } || {
        echo "Something went wrong."
      }
    - |
      if [ "$driftExitCode" != "0" ]; then
        echo "Something went wrong in drift detection unit testing.";
        exit 1;
      fi

# test cleanup_bucket when bucket exists
# expected result: bucket deleted
#TODO: implement

# test cleanup_bucket when no bucket exists
# expected result: pass with output that the bucket was not found
#TODO: implement

cleanup:unit:drift:s3:
  extends: .cleanup_aws
  variables:
    CFN_STAGING_S3_BUCKET: ${CI_COMMIT_SHORT_SHA}-${CI_PIPELINE_IID}-temp-bucket-drift # unique name to avoid collisions
  script: ./cfn-build/cleanup_bucket.sh $CFN_STAGING_S3_BUCKET
  needs:
    - unit:detect_drift

cleanup:unit:no_drift:s3:
  extends: .cleanup_aws
  variables:
    CFN_STAGING_S3_BUCKET: ${CI_COMMIT_SHORT_SHA}-${CI_PIPELINE_IID}-temp-bucket-no-drift # unique name to avoid collisions
  script: ./cfn-build/cleanup_bucket.sh $CFN_STAGING_S3_BUCKET
  needs:
    - unit:detect_no_drift

cleanup:drift:cfn:
  extends: .cleanup_aws
  variables:
    CFN_STACK_NAME: drift-stack-$CI_COMMIT_SHORT_SHA
  script: python3 cfn-utils/delete_stack.py --stack-name $CFN_STACK_NAME
  needs:
    - unit:detect_drift

cleanup:no_drift:cfn:
  extends: .cleanup_aws
  variables:
    CFN_STACK_NAME: no-drift-stack-$CI_COMMIT_SHORT_SHA
  script: python3 cfn-utils/delete_stack.py --stack-name $CFN_STACK_NAME
  needs:
    - unit:detect_no_drift
