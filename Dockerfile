## Build for AWS Lint

## specify your base image pulled from docker hub
FROM alpine:latest

## specify the group or individual maintaining image
LABEL maintainer="Mike Atkinson, George Rolston"

RUN apk update && apk upgrade && apk add --no-cache --update \
  bash \
  file \
  git \
  jq \
  nodejs \
  npm \
  python3 \
  python3-dev \
  unzip \
  zip \
&& rm -rf /var/lib/apt/lists/*

COPY requirements.txt .
RUN pip3 install -r requirements.txt

RUN npm install -g cfn-tail@latest && which cfn-tail

RUN mkdir -p /usr/lib/cfn_utils

COPY cfn-utils /usr/lib/cfn_utils
RUN chmod -R a+x /usr/lib/cfn_utils
COPY cfn-build /usr/lib/cfn_utils/cfn-build
COPY cfn-test /usr/lib/cfn_utils/cfn-test
COPY cfn-deploy /usr/lib/cfn_utils/cfn-deploy
COPY cfn-budget /usr/lib/cfn_utils/cfn-budget

RUN find /usr/lib/cfn_utils/ -type f -iname "*.sh" -exec chmod a+x {} \;

RUN ln -s /usr/lib/cfn_utils/cfn-build/cleanup_bucket.sh /usr/bin/cleanup_bucket && chmod a+x /usr/bin/cleanup_bucket
RUN ln -s /usr/lib/cfn_utils/cfn-build/cfn_package.sh /usr/bin/cfn_package && chmod a+x /usr/bin/cfn_package
RUN ln -s /usr/lib/cfn_utils/cfn-build/stage_templates_s3.sh /usr/bin/stage_templates_s3 && chmod a+x /usr/bin/stage_templates_s3

RUN ln -s /usr/lib/cfn_utils/cfn-test/lint-templates.sh /usr/bin/lint-templates && chmod a+x /usr/bin/lint-templates
RUN ln -s /usr/lib/cfn_utils/cfn-test/validate-json.sh /usr/bin/validate-json && chmod a+x /usr/bin/validate-json
RUN ln -s /usr/lib/cfn_utils/cfn-test/validate-yaml.sh /usr/bin/validate-yaml && chmod a+x /usr/bin/validate-yaml

RUN ln -s /usr/lib/cfn_utils/cfn-deploy/check_stack_final_status.sh /usr/bin/check_stack_final_status && chmod a+x /usr/bin/check_stack_final_status
RUN ln -s /usr/lib/cfn_utils/cfn-deploy/create_change_set.sh /usr/bin/create_change_set && chmod a+x /usr/bin/create_change_set
RUN ln -s /usr/lib/cfn_utils/cfn-deploy/delete_stack.sh /usr/bin/delete_stack && chmod a+x /usr/bin/delete_stack
RUN ln -s /usr/lib/cfn_utils/cfn-deploy/detect_changes.sh /usr/bin/detect_changes && chmod a+x /usr/bin/detect_changes
RUN ln -s /usr/lib/cfn_utils/cfn-deploy/get_stack_status.sh /usr/bin/get_stack_status && chmod a+x /usr/bin/get_stack_status

RUN ln -s /usr/lib/cfn_utils/cfn-budget/estimate_template_cost.sh /usr/bin/estimate_template_cost && chmod a+x /usr/bin/estimate_template_cost
